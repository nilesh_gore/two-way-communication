package com.smartiam.twc.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.SmsMessage
import android.util.Log
import com.smartiam.twc.database.DatabaseHelper

class MySMSBroadCastReceiver : BroadcastReceiver() {

    private var db: DatabaseHelper? = null

    override fun onReceive(context: Context?, intent: Intent?) {
        db = DatabaseHelper(
            context,
            DatabaseHelper.DATABASE_NAME, null,
            DatabaseHelper.DATABASE_VERSION
        )
        var body = ""
        val bundle = intent?.extras
        val pdusArr = bundle!!.get("pdus") as Array<*>
        val messages: Array<SmsMessage?> = arrayOfNulls(pdusArr.size)

        // if SMS is Long and contain more than 1 Message we'll read all of them
        for (i in pdusArr.indices) {
            messages[i] = SmsMessage.createFromPdu(pdusArr[i] as ByteArray)
        }
        val mobileNumber: String? = messages[0]?.originatingAddress
        Log.i("MySMSBroadCastReceiver", "MobileNumber =$mobileNumber")
        Log.i(
            "MySMSBroadCastReceiver",
            "isMobileNUmberAvailable : ${db!!.checkMobileNumber(mobileNumber!!)}"
        )
        if (db!!.checkMobileNumber(mobileNumber) > 0) {
            db!!.updateSmsStatusDone(mobileNumber)
        }
        val bodyText = StringBuilder()
        for (i in messages.indices) {
            bodyText.append(messages[i]?.messageBody)
        }
        body = bodyText.toString()
        if (body.isNotEmpty()) {
            // Do something, save SMS in DB or variable , static object or ....
            Log.i("MySMSBroadCastReceiver", "body = $body")
        }
    }

}