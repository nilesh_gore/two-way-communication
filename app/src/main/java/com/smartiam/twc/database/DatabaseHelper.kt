package com.smartiam.twc.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.smartiam.twc.model.GroupNumbers
import com.smartiam.twc.model.SmsNumbers

class DatabaseHelper(
    context: Context? = null,
    name: String? = DATABASE_NAME,
    factory: SQLiteDatabase.CursorFactory? = null,
    version: Int = DATABASE_VERSION
) : SQLiteOpenHelper(context, name, factory, version) {

    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "TWC_DB"

        const val TABLE_SMS_NUMBERS = "sms_number"
        const val KEY_SMS_NUMBERS_PRIMARY_ID = "id"
        const val KEY_SMS_NUMBERS_NUMBER = "mobile_number"
        const val KEY_DATE_TIME = "datetime"
        const val KEY_SMS_GROUP = "smsGroup"
        const val KEY_SMS_NUMBERS_STATUS =
            "sms_status" // 0 is Idle, 1 is SMS sent, 2 is SMS received.
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_SMS_NUMBERS_TABLE = "CREATE TABLE $TABLE_SMS_NUMBERS (" +
                "$KEY_SMS_NUMBERS_PRIMARY_ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "$KEY_SMS_NUMBERS_NUMBER TEXT NOT NULL," +
                "$KEY_SMS_GROUP TEXT NOT NULL," +
                "$KEY_DATE_TIME TEXT NOT NULL," +
                "$KEY_SMS_NUMBERS_STATUS TEXT NOT NULL)"
        db?.execSQL(CREATE_SMS_NUMBERS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        if (oldVersion < 0) {
            // Here you can add new table into old DB
            // db.execSQL("DROP TABLE IF EXISTS TABLE_NAME");
            // onCreate(db)
        }
    }

    fun addSmsNumbers(list: ArrayList<String>) {
        deleteAllSmsNumbers()
        val db = writableDatabase
        var j = 1
        var k = 1
        for (i in 0 until list.size) {
            if (j == 30) {
                j = 1
                k++
            } else {
                j++
            }
            try {
                val values = ContentValues()
                values.put(KEY_SMS_NUMBERS_NUMBER, list[i])
                values.put(KEY_SMS_NUMBERS_STATUS, "0")
                values.put(KEY_DATE_TIME, "")
                values.put(KEY_SMS_GROUP, k)
                db.insert(TABLE_SMS_NUMBERS, null, values)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        db.close()
    }

    fun getSmsNumbers(): ArrayList<SmsNumbers> {
        val list = ArrayList<SmsNumbers>()
        val db = writableDatabase
        val cursor = db.rawQuery("SELECT * FROM $TABLE_SMS_NUMBERS", null)
        if (cursor.moveToFirst()) {
            do {
                val id = cursor.getInt(0)
                val mobileNumber = cursor.getString(1)
                val group = cursor.getString(2)
                val dateTime = cursor.getString(3)
                val status = cursor.getString(4)
                list.add(SmsNumbers(id, mobileNumber, group, dateTime, status))
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return list
    }

    fun getGroupSmsCount(): ArrayList<GroupNumbers> {
        val list = ArrayList<GroupNumbers>()
        val db = writableDatabase
        val cursor = db.rawQuery("SELECT * FROM $TABLE_SMS_NUMBERS GROUP BY $KEY_SMS_GROUP", null)
        val i = 0
        if (cursor.moveToFirst()) {
            do {
                val id = cursor.getInt(0)
                val mobileNumber = cursor.getString(1)
                val group = cursor.getString(2)
                val dateTime = cursor.getString(3)
                val status = cursor.getString(4)
                list.add(GroupNumbers(id, group, dateTime))
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return list
    }

    fun getGroupNumbers(whichGroup: String): ArrayList<SmsNumbers> {
        val list = ArrayList<SmsNumbers>()
        val db = writableDatabase
        val cursor =
            db.rawQuery(
                "SELECT * FROM $TABLE_SMS_NUMBERS WHERE $KEY_SMS_GROUP='$whichGroup'",
                null
            )
        if (cursor.moveToFirst()) {
            do {
                val id = cursor.getInt(0)
                val mobileNumber = cursor.getString(1)
                val group = cursor.getString(2)
                val dateTime = cursor.getString(3)
                val status = cursor.getString(4)
                list.add(SmsNumbers(id, mobileNumber, group, dateTime, status))
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return list
    }

    fun getSmsNumbersPending(): ArrayList<SmsNumbers> {
        val list = ArrayList<SmsNumbers>()
        val db = writableDatabase
        val cursor =
            db.rawQuery(
                "SELECT * FROM $TABLE_SMS_NUMBERS WHERE $KEY_SMS_NUMBERS_STATUS='0' OR $KEY_SMS_NUMBERS_STATUS='1'",
                null
            )
        if (cursor.moveToFirst()) {
            do {
                val id = cursor.getInt(0)
                val mobileNumber = cursor.getString(1)
                val group = cursor.getString(2)
                val dateTime = cursor.getString(4)
                val status = cursor.getString(3)
                list.add(SmsNumbers(id, mobileNumber, group, dateTime, status))
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return list
    }

    fun getSmsNumbersDone(): ArrayList<SmsNumbers> {
        val list = ArrayList<SmsNumbers>()
        val db = writableDatabase
        val cursor =
            db.rawQuery("SELECT * FROM $TABLE_SMS_NUMBERS WHERE $KEY_SMS_NUMBERS_STATUS='2'", null)
        if (cursor.moveToFirst()) {
            do {
                val id = cursor.getInt(0)
                val mobileNumber = cursor.getString(1)
                val group = cursor.getString(2)
                val dateTime = cursor.getString(3)
                val status = cursor.getString(4)
                list.add(SmsNumbers(id, mobileNumber, group, dateTime, status))
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return list
    }

    fun checkMobileNumber(mobileNumber: String): Int {
        val db = writableDatabase
        val cursor = db.rawQuery(
            "SELECT * FROM $TABLE_SMS_NUMBERS WHERE $KEY_SMS_NUMBERS_NUMBER = '${
                mobileNumber.removeRange(
                    0,
                    3
                )
            }'",
            null
        )
        val count = cursor.count
        cursor.close()
        db.close()
        return count
    }

    fun updateSmsDateTimeForGroup(mobileNumber: String, group: String, dateTime: String) {
        val db = writableDatabase
        db.execSQL(
            "UPDATE $TABLE_SMS_NUMBERS SET $KEY_DATE_TIME = '$dateTime', $KEY_SMS_NUMBERS_STATUS = '1' WHERE $KEY_SMS_GROUP = '$group'"
        )
    }

    fun updateSmsStatusPending(mobileNumber: String) {
        val db = writableDatabase
        db.execSQL(
            "UPDATE $TABLE_SMS_NUMBERS SET $KEY_SMS_NUMBERS_STATUS = '1'  WHERE $KEY_SMS_NUMBERS_NUMBER = '${
                mobileNumber.removeRange(
                    0,
                    3
                )
            }'"
        )
    }

    fun updateSmsStatusDone(mobileNumber: String) {
        val db = writableDatabase
        db.execSQL(
            "UPDATE $TABLE_SMS_NUMBERS SET $KEY_SMS_NUMBERS_STATUS = '2'  WHERE $KEY_SMS_NUMBERS_NUMBER = '${
                mobileNumber.removeRange(
                    0,
                    3
                )
            }'"
        )
    }

    fun deleteAllSmsNumbers() {
        val db: SQLiteDatabase = writableDatabase
        db.execSQL("DELETE FROM $TABLE_SMS_NUMBERS")
        db.close()
    }

}