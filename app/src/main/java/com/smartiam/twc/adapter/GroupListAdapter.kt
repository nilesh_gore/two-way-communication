package com.smartiam.twc.adapter

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.os.Build
import android.telephony.SmsManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.smartiam.twc.R
import com.smartiam.twc.database.DatabaseHelper
import com.smartiam.twc.model.GroupNumbers
import com.smartiam.twc.model.SmsNumbers
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class GroupListAdapter constructor(
    context: Context?,
    private val groupNumbers: ArrayList<GroupNumbers>,
) : RecyclerView.Adapter<GroupListAdapter.ViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var progressDialog: ProgressDialog? = null
    private var db: DatabaseHelper? = null

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtGroup: TextView = itemView.findViewById(R.id.txtGroup)
        var txtDateTime: TextView = itemView.findViewById(R.id.txtDateTime)
        var btnSend: TextView = itemView.findViewById(R.id.btnSend)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.item_group_list, parent, false)
        val holder = ViewHolder(view)
        db = DatabaseHelper(
            parent.context,
            DatabaseHelper.DATABASE_NAME, null,
            DatabaseHelper.DATABASE_VERSION
        )
        holder.txtGroup.text = "Group No : ${viewType + 1}"
        holder.txtDateTime.text = "Date : ${groupNumbers[viewType].lastDateTime}"
        holder.btnSend.setOnClickListener {
            val mobileNumbers: ArrayList<SmsNumbers> =
                db!!.getGroupNumbers(groupNumbers[viewType].group)
            if (mobileNumbers.size > 0) {
                progressDialog = ProgressDialog.show(
                    it.context,
                    "Sending SMS..",
                    "Please Wait.. ",
                    true,
                    false
                )
                for (i in 0 until mobileNumbers.size) {
                    try {
                        val smsManager: SmsManager = SmsManager.getDefault()
                        smsManager.sendTextMessage(
                            mobileNumbers[i].mobileNumber,
                            null,
                            "Status",
                            null,
                            null
                        )
                        db!!.updateSmsDateTimeForGroup(
                            mobileNumbers[i].mobileNumber,
                            groupNumbers[viewType].group,
                            getCurrentDateTime()
                        )
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                progressDialog!!.dismiss()
            }
        }
        return holder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return groupNumbers.size
    }

    @SuppressLint("SimpleDateFormat")
    private fun getCurrentDateTime(): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")
            val answer: String = current.format(formatter)
            answer
        } else {
            val date = Date()
            val formatter = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
            val answer: String = formatter.format(date)
            answer
        }
    }

}