package com.smartiam.twc.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.telephony.SmsManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.smartiam.twc.R
import com.smartiam.twc.model.SmsNumbers

class MobileListAdapter constructor(
    context: Context?,
    private val mobileNumbers: ArrayList<SmsNumbers>,
) : RecyclerView.Adapter<MobileListAdapter.ViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtMobileNumber: TextView = itemView.findViewById(R.id.txtMobileNumber)
        var btnSend: TextView = itemView.findViewById(R.id.btnSend)
        var viewColor: View = itemView.findViewById(R.id.viewColor)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.item_mobile_list, parent, false)
        val holder = ViewHolder(view)
        holder.txtMobileNumber.text = "Mobile No : ${mobileNumbers[viewType].mobileNumber}"
        holder.btnSend.setOnClickListener {
            Toast.makeText(parent.context, "SMS sent", Toast.LENGTH_SHORT).show()
            val smsManager: SmsManager = SmsManager.getDefault()
            smsManager.sendTextMessage(
                mobileNumbers[viewType].mobileNumber,
                null,
                "Hii!!",
                null,
                null
            )
        }
        if (mobileNumbers[viewType].status == "0") {
            holder.viewColor.setBackgroundColor(
                ContextCompat.getColor(
                    parent.context,
                    R.color.blue_dark
                )
            )
        } else if (mobileNumbers[viewType].status == "1") {
            holder.viewColor.setBackgroundColor(
                ContextCompat.getColor(
                    parent.context,
                    R.color.red_dark
                )
            )
        } else if (mobileNumbers[viewType].status == "2") {
            holder.viewColor.setBackgroundColor(
                ContextCompat.getColor(
                    parent.context,
                    R.color.green_dark
                )
            )
        }
        return holder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return mobileNumbers.size
    }

}