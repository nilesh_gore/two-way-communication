package com.smartiam.twc.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.smartiam.twc.R
import com.smartiam.twc.database.DatabaseHelper
import com.smartiam.twc.utils.UtilsCSV

class MainActivity : AppCompatActivity() {
    private var db: DatabaseHelper? = null
    private var btnGroup: Button? = null
    private var btnCheckAll: Button? = null
    private var btnPending: Button? = null
    private var btnUploadNew: Button? = null
    private var btnExport: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        db = DatabaseHelper(
            applicationContext,
            DatabaseHelper.DATABASE_NAME, null,
            DatabaseHelper.DATABASE_VERSION
        )
        btnGroup = findViewById(R.id.btnGroup)
        btnCheckAll = findViewById(R.id.btnCheckAll)
        btnPending = findViewById(R.id.btnPending)
        btnUploadNew = findViewById(R.id.btnUploadNew)
        btnExport = findViewById(R.id.btnExport)

        btnGroup!!.setOnClickListener {
            if (db!!.getSmsNumbers().size > 0) {
                val intent = Intent(this@MainActivity, GroupListActivity::class.java)
                intent.putExtra("flag", "0")
                startActivity(intent)
            } else {
                Toast.makeText(applicationContext, "Please Upload Numbers", Toast.LENGTH_SHORT)
                    .show()
            }
        }
        btnCheckAll!!.setOnClickListener {
            if (db!!.getSmsNumbers().size > 0) {
                val intent = Intent(this@MainActivity, MobileListActivity::class.java)
                intent.putExtra("flag", "0")
                startActivity(intent)
            } else {
                Toast.makeText(applicationContext, "Please Upload Numbers", Toast.LENGTH_SHORT)
                    .show()
            }
        }

        btnUploadNew!!.setOnClickListener {
            val intent = Intent(this@MainActivity, FilePickupActivity::class.java)
            startActivity(intent)
        }

        btnExport!!.setOnClickListener {
            if (db!!.getSmsNumbers().size > 0) {
//                ExcelUtils.writeIntoExcelArray(db!!.getSmsNumbers(), applicationContext)
                UtilsCSV.openCSVWriter(db!!.getSmsNumbers(), this@MainActivity)
            } else {
                Toast.makeText(applicationContext, "Please Upload Numbers", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }
}