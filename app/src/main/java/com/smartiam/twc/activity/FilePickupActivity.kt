package com.smartiam.twc.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.Intent.ACTION_PICK
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI
import android.provider.MediaStore.Video.Media.INTERNAL_CONTENT_URI
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import br.com.onimur.handlepathoz.HandlePathOz
import br.com.onimur.handlepathoz.HandlePathOzListener
import br.com.onimur.handlepathoz.model.PathOz
import com.smartiam.twc.R
import com.smartiam.twc.database.DatabaseHelper
import com.smartiam.twc.utils.ExcelUtils.extractDataFromExcel
import jxl.Workbook
import java.io.File


class FilePickupActivity : AppCompatActivity(), HandlePathOzListener.SingleUri {
    val requestCode = 2021
    val REQUEST_OPEN_GALLERY = 1111
    var workbook: Workbook? = null
    var mobileNumbers: ArrayList<String>? = null
    var btnUploadNumbers: TextView? = null
    private lateinit var handlePathOz: HandlePathOz
    private var db: DatabaseHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_file_pickup)
        db = DatabaseHelper(
            applicationContext,
            DatabaseHelper.DATABASE_NAME, null,
            DatabaseHelper.DATABASE_VERSION
        )
        handlePathOz = HandlePathOz(this, this)
        btnUploadNumbers = findViewById(R.id.btnUploadNumbers)
        btnUploadNumbers!!.setOnClickListener {
            if (!checkPermissions()) {
                requestPermissions(requestCode)
            } else {
                val intent =
                    if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
                        Intent(ACTION_PICK, EXTERNAL_CONTENT_URI)
                    } else {
                        Intent(ACTION_PICK, INTERNAL_CONTENT_URI)
                    }

                intent.apply {
                    type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    action = Intent.ACTION_GET_CONTENT
                    action = Intent.ACTION_OPEN_DOCUMENT
                    addCategory(Intent.CATEGORY_OPENABLE)
                    putExtra("return-data", true)
                    addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                }
                startActivityForResult(intent, REQUEST_OPEN_GALLERY)
            }
        }
    }

    override fun onRequestHandlePathOz(pathOz: PathOz, tr: Throwable?) {
        Toast.makeText(
            this,
            "The real path is: ${pathOz.path} \n The type is: ${pathOz.type}",
            Toast.LENGTH_SHORT
        ).show()
        val file = File(pathOz.path)
        Toast.makeText(this, file.name, Toast.LENGTH_SHORT).show()
        fetchMobileNumbers(file)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((requestCode == REQUEST_OPEN_GALLERY) and (resultCode == Activity.RESULT_OK)) {
            data?.data?.also { it ->
                handlePathOz.getRealPath(it)
            }
        }
    }

    override fun onDestroy() {
        handlePathOz.onDestroy()
        super.onDestroy()
    }

    private fun fetchMobileNumbers(file: File) {
//        val ws = WorkbookSettings()
//        ws.gcDisabled = true
//        try {
//            workbook = Workbook.getWorkbook(file)
//            val sheet = workbook!!.getSheet(0)
//            for (i in 0 until sheet.rows) {
//                val row = sheet.getRow(i)
//                (mobileNumbers as ArrayList<String>).add(row[0].contents)
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
        mobileNumbers = extractDataFromExcel(file)
        if (mobileNumbers!!.size > 0) {
            db!!.addSmsNumbers(mobileNumbers!!)
//            val intent = Intent(this@FilePickupActivity, MobileListActivity::class.java)
            val intent = Intent(this@FilePickupActivity, GroupListActivity::class.java)
            intent.putExtra("flag", "0")
            startActivity(intent)
            finish()
        }
    }

    private fun checkPermissions(): Boolean {
        return (Build.VERSION.SDK_INT < Build.VERSION_CODES.M
                || (ContextCompat.checkSelfPermission(
            applicationContext, Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) + ContextCompat.checkSelfPermission(
            applicationContext, Manifest.permission.READ_EXTERNAL_STORAGE
        ) + ContextCompat.checkSelfPermission(
            applicationContext, Manifest.permission.SEND_SMS
        ) + ContextCompat.checkSelfPermission(
            applicationContext, Manifest.permission.READ_SMS
        ) + ContextCompat.checkSelfPermission(
            applicationContext, Manifest.permission.RECEIVE_SMS
        )) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermissions(requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.SEND_SMS,
                    Manifest.permission.READ_SMS,
                    Manifest.permission.RECEIVE_SMS,
                ), requestCode
            )
        }
    }


}