package com.smartiam.twc.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.smartiam.twc.R
import com.smartiam.twc.adapter.GroupListAdapter
import com.smartiam.twc.database.DatabaseHelper
import com.smartiam.twc.model.GroupNumbers

class GroupListActivity : AppCompatActivity() {

    private var mobileNumbers: ArrayList<GroupNumbers>? = null
    private var rvGroup: RecyclerView? = null
    private var toolbar: Toolbar? = null
    private var adapter: GroupListAdapter? = null
    private var db: DatabaseHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_list)
        db = DatabaseHelper(
            applicationContext,
            DatabaseHelper.DATABASE_NAME, null,
            DatabaseHelper.DATABASE_VERSION
        )
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        mobileNumbers = db!!.getGroupSmsCount()
        rvGroup = findViewById(R.id.rvGroup)
        showData()
    }

    private fun showData() {
        rvGroup!!.layoutManager = LinearLayoutManager(this@GroupListActivity)
        adapter = GroupListAdapter(this@GroupListActivity, mobileNumbers!!)
        rvGroup!!.adapter = adapter
    }
}