package com.smartiam.twc.activity

import android.app.ProgressDialog
import android.os.Bundle
import android.telephony.SmsManager
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.smartiam.twc.R
import com.smartiam.twc.adapter.MobileListAdapter
import com.smartiam.twc.database.DatabaseHelper
import com.smartiam.twc.model.SmsNumbers


class MobileListActivity : AppCompatActivity() {
    private var mobileNumbers: ArrayList<SmsNumbers>? = null
    private var rvMobileNumber: RecyclerView? = null
    private var toolbar: Toolbar? = null
    private var adapter: MobileListAdapter? = null
    private var db: DatabaseHelper? = null
    private var flag = "0"
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile_list)
        db = DatabaseHelper(
            applicationContext,
            DatabaseHelper.DATABASE_NAME, null,
            DatabaseHelper.DATABASE_VERSION
        )
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        flag = intent.extras!!.getString("flag")!!
        rvMobileNumber = findViewById(R.id.rvMobileNumber)
        when (flag) {
            "0" -> {
                mobileNumbers = db!!.getSmsNumbers()
            }
            "1" -> {
                mobileNumbers = db!!.getSmsNumbersPending()
            }
            "2" -> {
                mobileNumbers = db!!.getSmsNumbersDone()
            }
        }
        showData()
    }

    private fun showData() {
        rvMobileNumber!!.layoutManager = LinearLayoutManager(this@MobileListActivity)
        adapter = MobileListAdapter(this@MobileListActivity, mobileNumbers!!)
        rvMobileNumber!!.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_settings) {
            // To something
            if (mobileNumbers!!.size > 0) {
                progressDialog = ProgressDialog.show(
                    this,
                    "Sending SMS..",
                    "Please Wait.. ",
                    true,
                    false
                )
                for (i in 0 until mobileNumbers!!.size) {
                    try {
                        val smsManager: SmsManager = SmsManager.getDefault()
                        smsManager.sendTextMessage(
                            mobileNumbers!![i].mobileNumber,
                            null,
                            "Status",
                            null,
                            null
                        )
                        db!!.updateSmsStatusPending(mobileNumbers!![i].mobileNumber)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                progressDialog!!.dismiss()
            }
        } else if (id == R.id.action_export) {
//            try {
//                var workbook =
//                    WorkSheet.Builder(applicationContext, "CAT").setSheet(mobileNumbers).writeSheet()
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
        }
        return super.onOptionsItemSelected(item)
    }
}