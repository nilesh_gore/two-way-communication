package com.smartiam.twc.model

data class GroupNumbers(var id: Int, var group: String, var lastDateTime: String)
