package com.smartiam.twc.model

data class SmsNumbers(
    var id: Int,
    var mobileNumber: String,
    var group: String,
    var dateTime: String,
    var status: String
)
