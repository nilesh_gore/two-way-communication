package com.smartiam.twc.utils

import android.annotation.SuppressLint
import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import com.smartiam.twc.model.SmsNumbers
import org.apache.poi.hssf.usermodel.HSSFDateUtil
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.*
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.*
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList


object ExcelUtils {
    /**
     * Read Excel File
     * @param file
     * @throws FileNotFoundException
     */
    @Throws(FileNotFoundException::class)
    fun readExcel(file: File?) {
        if (file == null) {
            Log.e("NullFile", "read Excel Error, file is empty")
            return
        }
        val stream: InputStream = FileInputStream(file)
        try {
            val workbook = XSSFWorkbook(stream)
            val sheet = workbook.getSheetAt(0)
            val rowsCount = sheet.physicalNumberOfRows
            val formulaEvaluator: FormulaEvaluator =
                workbook.creationHelper.createFormulaEvaluator()
            for (r in 0 until rowsCount) {
                val row: Row = sheet.getRow(r)
                val cellsCount = row.physicalNumberOfCells
                //Read one line at a time
                for (c in 0 until cellsCount) {
                    //Convert the contents of each grid to a string
                    val value = getCellAsString(row, c, formulaEvaluator)
                    if (c == 2) {
                        val cellInfo = "r:$r; c:$c; v:$value"
                        Log.e("ExelUtil", cellInfo)
                    }
                }
            }
        } catch (e: Exception) {
            /* proper exception handling to be here */
            Log.e("ExelUtil", e.toString())
        }
    }

    fun extractDataFromExcel(file: File?): ArrayList<String> {
        var list = ArrayList<String>()
        if (file == null) {
            Log.e("NullFile", "read Excel Error, file is empty")
            return list
        }
        val stream: InputStream = FileInputStream(file)
        try {
            val workbook = XSSFWorkbook(stream)
            val sheet = workbook.getSheetAt(0)
            val rowsCount = sheet.physicalNumberOfRows
            val formulaEvaluator: FormulaEvaluator =
                workbook.creationHelper.createFormulaEvaluator()
            for (r in 0 until rowsCount) {
                val row: Row = sheet.getRow(r)
                val cellsCount = row.physicalNumberOfCells
                //Read one line at a time
                for (c in 0 until cellsCount) {
                    try {
                        //Convert the contents of each grid to a string
                        val value = getCellAsString(row, c, formulaEvaluator)
                        if (c == 2) {
                            val cellInfo = "r:$r; c:$c; v:$value"
                            Log.e("ExelUtil", cellInfo)
                            if (isNumber(value)) {
                                list.add(value)
                                Log.e("ExelUtil", "isNumber : TRUE")
                            }
                        }
                    } catch (e: Exception) {
                        Log.e("ExelUtil", e.toString())
                    }
                }
            }
        } catch (e: Exception) {
            Log.e("ExelUtil", e.toString())
        }
        return list
    }

    fun isNumber(s: String): Boolean {
        return when (s.toBigInteger()) {
            null -> false
            else -> true
        }
    }

    /**
     * Read the contents of each line in the excel file
     * @param row
     * @param c
     * @param formulaEvaluator
     * @return
     */
    private fun getCellAsString(row: Row, c: Int, formulaEvaluator: FormulaEvaluator): String {
        var value = ""
        try {
            val cell = row.getCell(c)
            val cellValue = formulaEvaluator.evaluate(cell)
            when (cellValue.cellType) {
                Cell.CELL_TYPE_BOOLEAN -> value = "" + cellValue.booleanValue
                Cell.CELL_TYPE_NUMERIC -> {
                    val numericValue = cellValue.numberValue
                    value = if (HSSFDateUtil.isCellDateFormatted(cell)) {
                        val date = cellValue.numberValue
                        val formatter = SimpleDateFormat("dd/MM/yy")
                        formatter.format(HSSFDateUtil.getJavaDate(date))
                    } else {
                        val fmt = DataFormatter()
                        "" + fmt.formatCellValue(cell)
                    }
                }
                Cell.CELL_TYPE_STRING -> value = "" + cellValue.stringValue
                else -> {
                }
            }
        } catch (e: NullPointerException) {
            /* proper error handling should be here */
            Log.e("ExelUtil", e.toString())
        }
        return value
    }

    /**
     * Simply determine whether an Excel file is an Excel file based on the type suffix name
     * @param file file
     * @return Is Excel File
     */
    fun checkIfExcelFile(file: File?): Boolean {
        if (file == null) {
            return false
        }
        val name = file.name
        //"."Escape characters are required
        val list = name.split("\\.").toTypedArray()
        //Less than two elements after partitioning indicate that the type name cannot be obtained
        if (list.size < 2) {
            return false
        }
        val typeName = list[list.size - 1]
        //Satisfies xls or xlsx to be able to
        return "xls" == typeName || "xlsx" == typeName
    }

    fun getPathFromURI(context: Context, uri: Uri): String? {
        //val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
        val isKitKat = false
        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":").toTypedArray()
                val type = split[0]
                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
            } else if (isDownloadsDocument(uri)) {
                val id = DocumentsContract.getDocumentId(uri)
                val contentUri: Uri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"),
                    java.lang.Long.valueOf(id)
                )
                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":").toTypedArray()
                val type = split[0]
                var contentUri: Uri? = null
                if ("image" == type) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                val selection = "_id=?"
                val selectionArgs = arrayOf(
                    split[1]
                )
                return getDataColumn(context, contentUri, selection, selectionArgs)
            }
        } else if ("content".equals(uri.scheme, ignoreCase = true)) {
            return getDataColumn(context, uri, null, null)
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    private fun getDataColumn(
        context: Context, uri: Uri?, selection: String?,
        selectionArgs: Array<String>?
    ): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(
            column
        )
        try {
            cursor = context.contentResolver.query(
                uri!!, projection, selection, selectionArgs,
                null
            )
            if (cursor != null && cursor.moveToFirst()) {
                val column_index: Int = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(column_index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    /**
     * Java method to write dates from Excel file in Java.
     * This method write value into .XLS file in Java.
     * @param file, name of excel file to write.
     * @throws IOException
     * @throws FileNotFoundException
     */
    @Throws(FileNotFoundException::class, IOException::class)
    fun writeIntoExcel(file: String?) {
        val book: Workbook = HSSFWorkbook()
        val sheet: Sheet = book.createSheet("Birthdays")

        // first row start with zero
        val row: Row = sheet.createRow(0)

        // we will write name and birthdates in two columns
        // name will be String and birthday would be Date
        // formatted as dd.mm.yyyy
        val name = row.createCell(0)
        name.setCellValue("John")
        val birthdate = row.createCell(1)

        // steps to format a cell to display date value in Excel
        // 1. Create a DataFormat
        // 2. Create a CellStyle
        // 3. Set format into CellStyle
        // 4. Set CellStyle into Cell
        // 5. Write java.util.Date into Cell
        val format: DataFormat = book.createDataFormat()
        val dateStyle: CellStyle = book.createCellStyle()
        dateStyle.dataFormat = format.getFormat("dd.mm.yyyy")
        birthdate.cellStyle = dateStyle

        // It's very trick method, deprecated, don't use
        // year is from 1900, month starts with zero
        birthdate.setCellValue(Date(110, 10, 10))

        // auto-resizing columns
        sheet.autoSizeColumn(1)

        // Now, its time to write content of Excel into File
        book.write(FileOutputStream(file))
        book.close()
    }

    fun writeIntoExcelArray(mobileNumbers: ArrayList<SmsNumbers>?, context: Context) {
        val name = "TWC Report ${getCurrentDateTime()}"
        val fileDirectory = File(
            context.getExternalFilesDir(null)!!.absolutePath +
                    "/exported_files/"
        )
        if (!fileDirectory.exists()) {
            fileDirectory.mkdir()
        }
        val file = File(fileDirectory, "$name.xlsx")
        if (!file.exists()) {
            file.createNewFile()
        }
        val book: Workbook = HSSFWorkbook()
        val sheet: Sheet = book.createSheet(name)
        if (mobileNumbers!!.size > 0) {
            for (i in 0 until (mobileNumbers.size + 1)) {
                if (i == 0) {
                    sheet.createRow(i).createCell(0).setCellValue("Id")
                    sheet.createRow(i).createCell(1).setCellValue("MobileNo")
                    sheet.createRow(i).createCell(2).setCellValue("Group")
                    sheet.createRow(i).createCell(3).setCellValue("date")
                    sheet.createRow(i).createCell(4).setCellValue("Status")
                } else {
                    val id = mobileNumbers[i - 1].id
                    val mobileNo = mobileNumbers[i - 1].mobileNumber
                    val group = mobileNumbers[i - 1].group
                    val date = mobileNumbers[i - 1].dateTime
                    val status = mobileNumbers[i - 1].status
                    sheet.createRow(i).createCell(0).setCellValue("$id")
                    sheet.createRow(i).createCell(1).setCellValue("$mobileNo")
                    sheet.createRow(i).createCell(2).setCellValue("$group")
                    sheet.createRow(i).createCell(3).setCellValue("$date")
                    when (status) {
                        "0" -> {
                            sheet.createRow(i).createCell(4).setCellValue("N/A")
                        }
                        "1" -> {
                            sheet.createRow(i).createCell(4).setCellValue("Acknowledge Pending")
                        }
                        "2" -> {
                            sheet.createRow(i).createCell(4).setCellValue("Acknowledged")
                        }
                    }
//                    sheet.autoSizeColumn(i)
                }
            }
        }
        // Now, its time to write content of Excel into File
        book.write(FileOutputStream(file))
        book.close()
    }

    @SuppressLint("SimpleDateFormat")
    private fun getCurrentDateTime(): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("ddMMyyyyHHmmss")
            val answer: String = current.format(formatter)
            answer
        } else {
            val date = Date()
            val formatter = SimpleDateFormat("ddMMyyyyHHmmss")
            val answer: String = formatter.format(date)
            answer
        }
    }

}