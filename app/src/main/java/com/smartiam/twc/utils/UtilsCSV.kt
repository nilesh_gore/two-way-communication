package com.smartiam.twc.utils

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import com.opencsv.CSVWriter
import com.smartiam.twc.model.SmsNumbers
import java.io.File
import java.io.FileWriter
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


object UtilsCSV {

    fun openCSVWriter(mobileNumbers: ArrayList<SmsNumbers>?, context: AppCompatActivity) {
        try {
            val name = "TWC Report ${getCurrentDateTime()}"
            val fileDirectory = File(
                context.getExternalFilesDir(null)!!.absolutePath +
                        "/exported_files/"
            )
            if (!fileDirectory.exists()) {
                fileDirectory.mkdir()
            }
            val file = File(fileDirectory, "$name.csv")
            if (!file.exists()) {
                file.createNewFile()
            }
            val outputfile = FileWriter(file)
            val writer = CSVWriter(outputfile)
            val data: MutableList<Array<String>> = ArrayList()
            for (i in 0 until mobileNumbers!!.size + 1) {
                if (i == 0) {
                    data.add(arrayOf("Id", "MobileNo", "Group", "date", "Status"))
                } else {
                    val id = mobileNumbers[i - 1].id
                    val mobileNo = mobileNumbers[i - 1].mobileNumber
                    val group = mobileNumbers[i - 1].group
                    val date = mobileNumbers[i - 1].dateTime
                    val value = mobileNumbers[i - 1].status
                    var status = "N/A"
                    when (value) {
                        "0" -> {
                            status = "N/A"
                        }
                        "1" -> {
                            status = "Acknowledge Pending"
                        }
                        "2" -> {
                            status = "Acknowledged"
                        }
                    }
                    data.add(arrayOf("$id", "$mobileNo", "$group", "$date", "$status"))
                }
            }
            writer.writeAll(data)
            writer.close()
            val sharingIntent = Intent(Intent.ACTION_SEND)
            val screenshotUri: Uri = Uri.parse(file.path)
            sharingIntent.type = "*/*"
            sharingIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri)
            (context).startActivity(Intent.createChooser(sharingIntent, "Share using"))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getCurrentDateTime(): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("ddMMyyyyHHmmss")
            val answer: String = current.format(formatter)
            answer
        } else {
            val date = Date()
            val formatter = SimpleDateFormat("ddMMyyyyHHmmss")
            val answer: String = formatter.format(date)
            answer
        }
    }

}